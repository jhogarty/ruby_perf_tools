require 'benchmark/ips'

def use_strings
  array = []
  1_000_000.times do
   array << "a sample string"
  end
end

def use_symbols
  array = []
  1_000_000.times do
    array << :some_symbol_here
  end
end

Benchmark.ips do |x|
  x.report('hares go fast') { use_symbols }
  x.report('turtles go slow') { use_strings }
  x.compare!
end

