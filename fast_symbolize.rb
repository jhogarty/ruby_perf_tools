require 'benchmark/ips'

class Hash

  def symbolize_keys(&select)
    dup.symbolize_keys!(&select)
  end

  def symbolize_keys!(&select)
    if select
      keys.each do |key|
        if select[key]
          new_key = (key.to_sym rescue key.to_s.to_sym)
          self[new_key] = delete(key)
        end
      end
    else
      keys.each do |key|
        new_key = (key.to_sym rescue key.to_s.to_sym)
        self[new_key] = delete(key)
      end
    end
    self
  end

end

@params = {'thing' => 'value1', 'dodad' => {'bobot' => 'nurny', 'fizbot' => 'boxcar'},
          'jared' => {'car' => 'alex', 'dog' => 'balls'}, 'odo' => 'quark'}

def symbolize_keyz(hash)
  hash.inject({}){|result, (key, value)|
    new_key = case key
              when String then key.to_sym
              else key
              end
    new_value = case value
                when Hash then symbolize_keyz(value)
                else value
                end
    result[new_key] = new_value
    result
  }
end

def bolize_it
  50_000.times do
    @params.symbolize_keys
  end
end

def symb_it
  50_000.times do
    symbolize_keyz(@params)
  end
end

Benchmark.ips do |x|
  x.report('hares go fast') { symb_it }
  x.report('turtles go slow') { bolize_it }
  x.compare!
end

