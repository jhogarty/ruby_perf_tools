require 'memory_profiler'

MemoryProfiler.start

# -------------------------------------
  string_array = []
  1_000_000.times do
   string_array << "a sample string"
  end

  symbol_array = []
  1_000_000.times do
    symbol_array << :some_symbol_here
  end
# -------------------------------------

report = MemoryProfiler.stop
report.pretty_print(to_file: 'mem_prof_results.txt', color_output: 1, detailed_report: 1)

