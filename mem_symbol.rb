require "get_process_mem"

GC.start
allocated_before = GC.stat(:total_allocated_objects)
freed_before = GC.stat(:total_freed_objects)

mem = GetProcessMem.new
puts "Memory usage before: #{mem.mb} MB."


# some code to test
array = []
1_000_000.times do
 array << :some_symbol_here
end


mem = GetProcessMem.new
puts "Memory usage after: #{mem.mb} MB."

GC.start
allocated_after = GC.stat(:total_allocated_objects)
freed_after = GC.stat(:total_freed_objects)
puts "Total objects allocated: #{allocated_after - allocated_before}"
puts "Total objects freed: #{freed_after - freed_before}"
